import React from 'react'
import { Container} from "react-bootstrap";

function Copyright() {
    return (
        <Container fluid className="container-fluid copyright" style={{backgroundColor:"#4643d3", paddingTop:"17px", paddingBottom:"17px",textAlign:"center", color:"#fff" }}>&copy; 2020 PT BINAR BANK INDONESIA </Container>
    )
}

export default Copyright
