import React from 'react'
import { Container, Row, Col} from "react-bootstrap";

export default function ServiceProgram() {
  return (
    <Container  className="container-fluid services" style={{marginTop:"100px", marginBottom:"100px", textAlign:"center", alignSelf:"center"}}>
        <h3>
          Pelayanan Terbaik <span>Dari Kami</span>
        </h3>
        <Row className="pt-5">
          <Col xs={12} md={4}>
            <div className="service-card">
          <img src="/ser1.png" width="350px" style={{marginBottom:"16.3px"}}/>
            <h4 style={{marginBottom:"23px"}}>KALKULATOR</h4>
            <p>Hitung kemampuan <br/> menabung/cicilan, setoran tabungan DP, dan cicilan KPR setiap bulan</p>
            </div>
          </Col>
          <Col xs={12} md={4}>
          <div className="service-card">
          <img src="/ser2.png" width="356px" style={{marginBottom:"16.3px"}}/>
            <h4 style={{marginBottom:"23px"}}>TABUNGAN DAN KPR</h4>
            <p>Kemudahan menabung DP dan pengajuan KPR dalam satu aplikasi</p></div>
          </Col>
          <Col xs={12} md={4}>
          <div className="service-card">
          <img src="/ser3.png" width="356px" style={{marginBottom:"16.5px"}}/>
            <h4 style={{marginBottom:"23px"}}>KEAMANAN</h4>
            <p>Layanan pengaduan 24 jam</p></div>
          </Col>
     </Row>
      </Container>
  )
}
