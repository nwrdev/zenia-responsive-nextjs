import React from "react";
import { Container, Row, Col,Media } from "react-bootstrap";

function tabungan() {
  return (
      <Container fluid className="container-fluid tabungan"  id="tabungan"
      className="tabungan-section"
      style={{
        backgroundImage: "url(" + `${require("../../public/tab-bg.png")}` + ")",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}>
        <Container>
        <Row>
          <Col>
          <h3 style={{marginBottom:"30px"}}>
              Tabungan <span>DP Hunian</span>{" "}
            </h3>
      <ul className="list-unstyled">
        <Media as="li" style={{marginBottom:"30px"}}>
          <img width={64} height={64} className="mr-3"src="/tab-1.png" alt="Generic placeholder"/>
          <Media.Body>
          <h5 style={{fontWeight:"bold"}}>Hitung Kemampuan</h5>
                <p>
                  Awali dari hasil perhitungan kalkulator untuk memperoleh
                  rekomendasi perencanaan yang tepat
                </p>
          </Media.Body>
        </Media>
        <Media as="li" style={{marginBottom:"30px"}}>
          <img width={64} height={64} className="mr-3" src="/tab-2.png" alt="Generic placeholder"/>
          <Media.Body>
          <h5 style={{fontWeight:"bold"}}>Rekomendasi Tabungan</h5><p>Rekomendasi tabungan DP hunian dengan template yang sudah tersedia atau custom sesuai dengan kebutuhan kamu</p>
          </Media.Body>
        </Media>
        <Media as="li">
          <img width={64} height={64} className="mr-3" src="/tab-3.png" alt="Generic placeholder"/>
          <Media.Body>
          <h5 style={{fontWeight:"bold"}}>Setoran Awal Fleksibel</h5><p>Setoran awal dapat kamu sesuaikan dengan kemampuan kamu</p>
          </Media.Body>
        </Media>
      </ul>
          </Col>
          <Col>
            <div className="tabungan-image">
              <img src="/tabungan.png" width="600px" />
            </div>
          </Col>
        </Row>
      </Container>
      </Container>
  );
}

export default tabungan;
