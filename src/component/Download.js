import React from 'react'
import { Container, Row, Col} from "react-bootstrap";

function Download() {
    return (
       <Container className="container-fluid download" style={{marginTop:"30px" }}>
           <Row>
               <Col><h3 style={{marginBottom:"25px", width:"90%" }}>
               We Help You <span>Make Your Dream Come True</span>
            </h3>
            <p>Lengkapi kehidupanmu dan wujudkan mimpimu bersama kami. Zenia akan membantumu merencakanan impianmu untuk memiliki rumah dari muda</p>
            <h4 style={{color:"000",fontWeight:"bold",fontSize:"20px",marginTop:"40px", marginBottom:"24px"}}>Download Sekarang</h4>
            <a href="https://play.google.com/" target="_blank"><img src="/googleplay.png" width="192px" /></a>
            </Col>
               <Col><div className="download-phone"><img  src="/phone.png" width="452px" /></div></Col>
           </Row>
       </Container>
    )
}

export default Download
