import React from 'react'
import {Carousel} from "react-bootstrap";

export default function BannerSlider() {
  return (
    <Carousel>
    <Carousel.Item>
      <img
        className="d-block w-100"
        src="/bn1.png"
        alt="First slide"
      />
    </Carousel.Item>
    <Carousel.Item>
      <img
        className="d-block w-100"
        src="/bn2.png"
        alt="Third slide"
      />
    </Carousel.Item>
    <Carousel.Item>
      <img
        className="d-block w-100"
        src="/bn3.png"
        alt="Third slide"
      />
    </Carousel.Item>
  </Carousel>
  )
}
