import React from "react";
import { Container, Row, Col} from "react-bootstrap";


function Kpr() {
  return (
   <Container id="kpr" fluid className="container-fluid kpr" style={{backgroundColor:"#f0f8ff"}}>
     <Container>
     <Row>
       <Col style={{alignContent:"center"}}><img src="kprMain-1.png" width="550px" /></Col>
       <Col><h3 style={{marginTop:"100px"}}>Kemudahan<br/><span>Pengajuan KPR</span></h3><p>Kamu bisa melakukan pengajuan KPR secara online tanpa ribet. Proses pengajuan KPR ini bisa dilakukan dimana saja dengan melampirkan beberapa berkas pendukung sebagai syarat yang tersedia di aplikasi. Permudah kehidupanmu dengan Zenia.</p></Col>
     </Row>
     </Container>
   </Container>
  );
}

export default Kpr;
